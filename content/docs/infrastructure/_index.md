---
title: "Setup local"
description: ""
---

# Troubleshooting
### Faire la convertion de WSL2 vers WSL1
- Ouvrir un **Powershell** ou **cmd**
- Exécuter la commande: `wsl --list`
- Passer la version que vous utilisez en WSL1: `wsl --set-version <nom_version> 1`. (exemple: `Ubuntu-22.04`) Il sera possible de revenir 
à WSL2 pour cette version en exécutant la commande avec le numéro de version à 2: `wsl --set-version <nom_version> 2`. Cette
conversion peut prendre plusieurs minutes.
- Entrer dans wsl: `wsl -d <nom_version>`

### Simuler les droits Windows sur WSL
- Se mettre à l'extérieur de la partition C: `cd ~`
- Démonter et remonter le C: sur WSL avec les droits Windows simulés: `sudo umount /mnt/c && sudo mount -t drvfs C: /mnt/c -o metadata`

### Tester le setup
- **Dans Windows**, ouvrir un **Powershell** ou **cmd**
- Mettez vous dans le répertoire du tp: `cd /path/to/tp-ansible`
- Exécuter `vagrant up frontend`
- **Dans WSL**, mettez vous dans le répertoire du tp: `cd /path/to/tp-ansible`
- Exécuter la commande suivante: `ssh -i .vagrant/machines/frontend/virtualbox/private_key -o IdentitiesOnly=yes root@localhost -p 3221`
- Vous devriez avoir accès à la machine

[//]: # (**Installer un WSL1 Ubuntu:**)
[//]: # (- Ouvrir un **Powershell** ou **cmd**)
[//]: # (- Exécuter la commande: `wsl --set-default-version 1` pour que les prochaines distributions soient installées en WSL1)
[//]: # (- Exécuter la commande: `wsl --list`)
[//]: # (- Installer une version que vous n'avez pas parmis *Ubuntu-20.04*, *Ubuntu-22.04*: `wsl --install -d <version>`)
[//]: # (- Entrer un nom d'utilisateur et un mot de passe)
[//]: # ()
[//]: # (**Simuler les droits d'accès Windows du système du fichier sur WSL:**)
[//]: # (- Exécuter le **WSL** installé à l'étape précédente: `wsl -d <version>`)
[//]: # (- Sortir du système de fichier Windows: `cd ~`)
[//]: # (- Démonter et remonter le C: sur WSL avec les droits simulés: `sudo umount /mnt/c && sudo mount -t drvfs C: /mnt/c -o metadata`)
[//]: # ()
[//]: # (**Installer pip**: )
[//]: # (- Installation: `curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && python3 get-pip.py`)
[//]: # (- Vérification: ``)
[//]: # ()
[//]: # (**Installer ansible**: `python3 -m pip install ansible`)

# Pour ceux sur Windows et Mac M1: Inventaire Openstack
Dans votre repo de TP (`tp-ansible`), créer un nouvel inventaire appelé: `inventories/openstack.yaml` 
```
all:
  hosts:
    frontend:
      ansible_host: <ip_frontend>
      ansible_user: root
      ansible_port: 22
      ansible_password: "0000"
    productcatalogservice:
      ansible_host: <ip_productcatalogservice>
      ansible_user: root
      ansible_port: 22
      ansible_password: "0000"
    recommendationservice:
      ansible_host: <ip_recommendationservice
      ansible_user: root
      ansible_port: 22
      ansible_password: "0000"
    cartservice:
      ansible_host: <ip_cartservice>
      ansible_user: root
      ansible_port: 22
      ansible_password: "0000"
```

Pour tous vos futurs appels Ansible, préciser l'inventaire openstack plutôt que vagrant. Au lieu de la commande:

`ansible-playbook -i inventories/vagrant.yaml <nom_playbook>`

Utiliser:

`ansible-playbook -i inventories/openstack.yaml <nom_playbook>`

Voici la liste des IP flottantes correspondantes aux 4 VMs, nous assignerons un groupe par groupes d'IPs.
```
floating_ip_addresses = {
  "fila2-devops-01" = [
    "10.29.247.103",
    "10.29.244.154",
    "10.29.245.99",
    "10.29.247.203",
  ]
  "fila2-devops-02" = [
    "10.29.245.12",
    "10.29.246.151",
    "10.29.247.98",
    "10.29.245.163",
  ]
  "fila2-devops-03" = [
    "10.29.247.199",
    "10.29.247.30",
    "10.29.245.102",
    "10.29.247.66",
  ]
  "fila2-devops-04" = [
    "10.29.246.30",
    "10.29.246.92",
    "10.29.245.110",
    "10.29.244.4",
  ]
  "fila2-devops-05" = [
    "10.29.246.104",
    "10.29.246.113",
    "10.29.245.214",
    "10.29.244.222",
  ]
  "fila2-devops-06" = [
    "10.29.245.23",
    "10.29.246.240",
    "10.29.246.105",
    "10.29.244.168",
  ]
  "fila2-devops-07" = [
    "10.29.246.177",
    "10.29.244.161",
    "10.29.245.67",
    "10.29.245.36",
  ]
  "fila2-devops-08" = [
    "10.29.246.97",
    "10.29.247.194",
    "10.29.244.83",
    "10.29.244.15",
  ]
}
```


# Pour ceux sur Windows: Inventaire Vagrant
Ajouter les lignes avec une flêche commentée dans votre fichier `Vagrantfile`:
```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # Doc: https://docs.vagrantup.com.

  config.vm.box = "http://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-vagrant.box"
  config.vm.provider "virtualbox" do |vb|
    vb.cpus = 2
    vb.memory = "512"
  end

  config.vm.define "frontend" do |frontend|
    frontend.vm.hostname = "frontend"
    frontend.vm.network "private_network", ip: "192.168.56.20"
    frontend.vm.network "forwarded_port", guest: 22, host: 3221
    frontend.vm.network "forwarded_port", guest: 8080, host: 9090
    frontend.ssh.port = 3221
    frontend.ssh.insert_key = false   # <----------------------------------
  end
  config.vm.define "pc" do |productcatalogservice|
    productcatalogservice.vm.hostname = "productcatalogservice"
    productcatalogservice.vm.network "private_network", ip: "192.168.56.21"
    productcatalogservice.vm.network "forwarded_port", guest: 22, host: 3222
    productcatalogservice.ssh.port = 3222
    productcatalogservice.ssh.insert_key = false   # <----------------------------------
  end
  config.vm.define "rc" do |recommandation|
    recommandation.vm.hostname = "recommandation"
    recommandation.vm.network "private_network", ip: "192.168.56.22"
    recommandation.vm.network "forwarded_port", guest: 22, host: 3223
    recommandation.ssh.port = 3223
    recommandation.ssh.insert_key = false   # <----------------------------------
  end
  config.vm.define "ct" do |cart|
    cart.vm.hostname = "cart"
    cart.vm.network "private_network", ip: "192.168.56.23"
    cart.vm.network "forwarded_port", guest: 22, host: 3224
    cart.ssh.port = 3224
    cart.ssh.insert_key = false   # <----------------------------------
  end

  config.vm.provision "shell", inline: <<-SHELL
    cp /home/vagrant/.ssh/authorized_keys /root/.ssh/authorized_keys
  SHELL
end
```

Modifiez votre fichier d'inventaire:
```
all:
  hosts:
    frontend:
      ansible_host: localhost
      ansible_user: root
      ansible_port: 3221
      ansible_ssh_private_key_file: "/mnt/c/Users/<nom_utilisateur>/.vagrant.d/insecure_private_key"

    productcatalogservice:
      ansible_host: localhost
      ansible_user: root
      ansible_port: 3222
      ansible_ssh_private_key_file: "/mnt/c/Users/<nom_utilisateur>/.vagrant.d/insecure_private_key"

    recommendationservice:
      ansible_host: localhost
      ansible_user: root
      ansible_port: 3223
      ansible_ssh_private_key_file: "/mnt/c/Users/<nom_utilisateur>/.vagrant.d/insecure_private_key"

    cartservice:
      ansible_host: localhost
      ansible_user: root
      ansible_port: 3224
      ansible_ssh_private_key_file: "/mnt/c/Users/<nom_utilisateur>/.vagrant.d/insecure_private_key"
```
