---
title: "Introduction"
description: ""
---

# Architecture de l'application fil rouge
L'objectif du TP est le déploiement via Ansible de l'application en microservice introduite dans le cours:
![Application fil rouge](/architecture_fil_rouge.png)
*Architecture application fil rouge*

# Déploiement de l'architecture
## Instructions de déploiement
Chaque microservice détient son propre **README** contenant les instructions de son déploiement. C'est à vous de trouver quels modules Ansible utiliser à chaque étape et quels paramètres leurs donner.
Une liste des modules Ansible à utiliser vous est proposée pour vous donner la liste des modules conseillés pour le déploiement des microservices

## Méthodologie
Il est conseillé de **vérifier** chaque étape avant de passer à la suivante. Pour cela, vous devrez vous connecter à la VM sur laquelle le déploiement est réalisé et tester.
Pour se connecter à la VM: `ssh -i .vagrant/machines/<vm_vagrant_name>/virtualbox/private_key -o IdentitiesOnly=yes root@localhost -p <port_vm>`.

Arguments par service:
- frontend:
  - vm_vagrant_name: `frontend`
  - port_vm: `3221`
- productcatalogservice:
  - vm_vagrant_name: `pc`
  - port_vm: `3222`
- recommandation:
  - vm_vagrant_name: `rc`
  - port_vm: `3223`
- cart:
  - vm_vagrant_name: `ct`
  - port_vm: `3224`


Si vous souhaitez repartir de zéro, vous pouvez détruire et reconstruire toutes les VMs via les commandes Vagrant: ```vagrant destroy```, ```vagrant up``` (voir README sur le repo du TP).

## Récupération des sources
Clonez dans un premier temps chaque microservices sur votre machine locale pour avoir accès aux **README** de chaque service. Utilisez les URLs suivantes:
- **cartservice**: https://gitlab.inria.fr/ue-devops-fila2/online-boutique-services/cartservice
- **frontend**: https://gitlab.inria.fr/ue-devops-fila2/online-boutique-services/frontend
- **productcatalogservice**: https://gitlab.inria.fr/ue-devops-fila2/online-boutique-services/productcatalogservice
- **recommendationservice**: https://gitlab.inria.fr/ue-devops-fila2/online-boutique-services/recommendationservice

Adresses IMT:
- **cartservice**: https://gitlab+deploy-token-36:HKhMunDUJMdNqKaDzMVQ@gitlab.imt-atlantique.fr/ue-devops-fila2/online-boutique-services/cartservice
- **frontend**: https://gitlab+deploy-token-34:rcyUc-DqpMaoCmP62dfS@gitlab.imt-atlantique.fr/ue-devops-fila2/online-boutique-services/frontend
- **productcatalogservice**: https://gitlab+deploy-token-33:TCe6Hux5sq81g6jmEsub@gitlab.imt-atlantique.fr/ue-devops-fila2/online-boutique-services/productcatalogservice
- **recommendationservice**: https://gitlab+deploy-token-37:TC4cPWqPPs2R8qTY5mir@gitlab.imt-atlantique.fr/ue-devops-fila2/online-boutique-services/recommendationservice

_Note_: La syntaxe ```https://<token_name>:<token_value>@gitlab.imt-atlantique.fr``` contient respectivement le nom et la valeur d'un deploy token associé au repository.
Ce token permet de s'authentifier sur Gitlab sans avoir à passer par une authentification personnelle. C'est une pratique courante des scripts d'automatisation, et 
c'est également pratique dans le cadre du TP pour faciliter la récupération des sources.

Lorsque vous passerez au TP, vous devrez utiliser **ces mêmes URLs** pour chaque microservice pour cloner les repos sur vos VMs.

## Déploiement des VMs via Vagrant
Exécuter la commande `vagrant up` dans le dossier contenant le Vagrantfile pour booter toutes les VMs.\
Si votre machine est lente, vous pouvez ne spécifier que certaines VMs, comme `vagrant up frontend pc` (boot des 2 VMs frontend et productcatalogservice)
