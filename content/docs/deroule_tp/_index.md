---
title: "Plan des séances"
description: ""
---

# Séance 1
- Cours d'introduction
- Installation des environnements 
  - Vagrant
  - Virtualbox
  - WSL, Ansible
- Fork du dépôt du TP
- Démo
- Déroulé du TP: Déploiement de l'architecture, Rédaction des scripts Ansible
- Réalisation
- Conclusion, prise de recul
