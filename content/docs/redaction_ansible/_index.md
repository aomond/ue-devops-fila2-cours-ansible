---
title: "Rédaction des scripts Ansible"
description: ""
---

# Rédaction des scripts Ansible
## Modules Ansible conseillés par microservice
Ensemble des modules builtin Ansible: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html#modules.\
Nous n'en utiliserons qu'une sous-partie.

### **Cart**
- [git](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html#ansible-collections-ansible-builtin-git-module)
- [apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module)
- [template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html#ansible-collections-ansible-builtin-template-module)
- [systemd](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html#ansible-collections-ansible-builtin-systemd-module)
- [command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html#ansible-collections-ansible-builtin-command-module) (uniquement s'il n'existe pas de façon de faire avec les modules donnés)

### **Frontend**
- [git](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html#ansible-collections-ansible-builtin-git-module)
- [unarchive](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/unarchive_module.html#ansible-collections-ansible-builtin-unarchive-module)
- [file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#ansible-collections-ansible-builtin-file-module)
- [apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module)
- [template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html#ansible-collections-ansible-builtin-template-module)
- [systemd](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html#ansible-collections-ansible-builtin-systemd-module)
- [command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html#ansible-collections-ansible-builtin-command-module) (uniquement s'il n'existe pas de façon de faire avec les modules donnés)

### **Product catalog**
- [git](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html#ansible-collections-ansible-builtin-git-module)
- [unarchive](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/unarchive_module.html#ansible-collections-ansible-builtin-unarchive-module)
- [file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#ansible-collections-ansible-builtin-file-module)
- [apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module)
- [template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html#ansible-collections-ansible-builtin-template-module)
- [systemd](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html#ansible-collections-ansible-builtin-systemd-module)
- [command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html#ansible-collections-ansible-builtin-command-module) (uniquement s'il n'existe pas de façon de faire avec les modules donnés)

### **Recommendation service**
- [git](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html#ansible-collections-ansible-builtin-git-module)
- [apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module)
- [pip](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pip_module.html#ansible-collections-ansible-builtin-pip-module)
- [template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html#ansible-collections-ansible-builtin-template-module)
- [systemd](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html#ansible-collections-ansible-builtin-systemd-module)
- [command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html#ansible-collections-ansible-builtin-command-module) (uniquement s'il n'existe pas de façon de faire avec les modules donnés)

## Lancement du service
Pour lancer chaque service, nous utiliserons `systemd`. Il s'agit d'un gestionnaire de services Linux permettant de démarrer, arrêter et monitorer des services utilisateur ou système. 
Cet outil se base sur un fichier de configuration décrivant le service à lancer, qui peut être démarré à la demande de l'utilisateur.
Voici le template à utiliser pour chaque service, ce template s'appelera `online_boutique_service.service.j2`:
```
[Unit]
Description=Lancement du microservice {{ service_name }}
After=network.target

[Service]
User=root
WorkingDirectory={{ working_directory }}
ExecStart={{ cmd_to_execute }}
Environment={{ env_vars }}

[Install]
WantedBy=multi-user.target
```
Il s'agit d'un template d'un fichier de configuration `systemd`. Ce template **devra être compilé et le résultat déposé sur la machine distante sous le chemin suivant**: `/etc/systemd/system/<service_name>.service` 
Via Ansible, les variables seront **automatiquement** complétées lors de l'appel du module correspondant dans la tâche.
Ces variables devront donc être définies en amont avant l'exécution du playbook Ansible, tel que:
- `service_name`: Le nom du service à lancer 
- `working_directory`: emplacement duquel exécuter la commande (dans notre cas, il s'agit du code source du service) 
- `cmd_to_execute`: commande à exécuter. Pour le cart, Le lancement du microservice se fait simplement en exécutant le binaire : `/path/to/cartservice` (binaire)
- `env_vars`: Les variables d'environnements dont a besoin le service. La string vide "" s'il n'y en n'a pas.

Lancer le service systemd en appelant le service `{{ service_name }}`. Mettre l'option `daemon_reload: true` de façon à dire à `systemd` de recharger le nouveau fichier de config avant chaque lancement du service.

**Vérifier** que le service est lancé: `systemctl status {{ service_name }}`

**Analyser** les logs en cas de problème: `journalctl -u {{ service_name }}`

## Dépendances entre les services
Les microservices écoutent sur les ports suivants:
- cartservice: **7070**
- frontend: **8080** (écoute locale sur le port 9090 forwardé en utilisant Vagrant)
- productcatalogueservice: **3550**
- recommendationservice: **8081** 

Le **frontend** dépend des trois autres microservices. Au lancement du frontend, les variables d'environnement suivantes doivent être définies:
- `PRODUCT_CATALOG_SERVICE_ADDR=<ip_productcatalogservice>:<port_productcatalogservice>`
- `CART_SERVICE_ADDR=<ip_cartservice>:<port_cartservice>`
- `RECOMMENDATION_SERVICE_ADDR=<ip_recommendationservice>:<port_recommendationservice>`

Le **service de recommandation** dépend du catalogue de produit. Au lancement du service de recommandation, les variables d'environnement suivantes doivent être définies:
- `PRODUCT_CATALOG_SERVICE_ADDR=<ip_productcatalogservice>:<port_productcatalogservice>`

Au lancement d'un playbook, Ansible récupère automatiquement des informations sur l'infrastructure cible. Parmi ces informations,
on trouve l'adresse IP des machines cibles. Ces adresses IP **ne sont nécessairement celles mentionnées dans l'inventaire**. Ces dernières ne servent que pour la connexion entre 
votre machine local et vos VMs. Pour obtenir l'adresse IP d'un service, nous pouvons la récupérer dynamiquement depuis les facts et la mettre dans les variables d'environnements:
- **Sur Openstack**: `{{ hostvars[<service_name>]['ansible_default_ipv4']['address'] }}`
- **Sur Vagrant**: `{{ hostvars[<service_name>]['ansible_enp0s8']['ipv4']['address'] }}`


## Redéploiement en cas de changement dans la configuration
Le **redéploiement** de l'application doit se faire pour deux raisons:
- Lorsque des changements sont detectés dans le repo git
- Lorsque la configuration du service `systemd` change (le template donné dans la partie **Lancement du service**)

Pour ce faire, vous devrez utiliser les `handlers` introduits dans le cours.\
Rappel: pour obtenir un **=**, vous devez prendre en compte le redéploiement.

## Notes diverses concernant l'installation des microservices
Pour ce TP, vous n'avez **pas besoin** d'ajouter dans le playbook l'installation de paquets déjà installés sur la VM. Exemple: Python3.10 est pré-installé sur Ubuntu22.04.
Vous n'avez donc pas besoin de rajouter cette installation si elle est requise pour le microservice.

### Cart
- Vous pouvez obtenir le numéro de version d'Ubuntu via les facts Ansible: `"{{ hostvars[inventory_hostname]['ansible_distribution_version'] }}"`
- `dpkg -i` peut être **dans notre cas** être assimilé à `apt install`

## Ordre de précédence des variables Ansible
https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#understanding-variable-precedence
