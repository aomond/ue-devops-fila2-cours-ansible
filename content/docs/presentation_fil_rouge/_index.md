---
title: "Evaluation"
description: ""
---
# Evaluation
Critères de notation pour le -, = et +:

## Rendu TP
| -                                                             | =                                                                                                                                                                                                                                                   | +                                                                                                                                                                                                                                                 |
|---------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| L'application **n'est pas déployée** ou ne **fonctionne pas** | L'application est **déployée**, ses fonctionnalités sont **présentes** et l'application est **redéployée** lorsque la configuration change. | Le code commun est **factorisé et réutilisé** dans des roles. Les variables sont **bien nommées** et **placées aux bons endroits** (playbook, roles, inventaire). Les playbooks exécutent les actions similaires **en parallèle** sur les noeuds. |

Le rendu se fait **par binôme**. Vous pouvez faire le TP chacun sur votre machine, mais à la fin un seul rendu par binôme devra être donné.\
Ce rendu sera l'export de votre repo.

**Dans votre README, pensez à indiquer le nom des personnes du binôme, ainsi que le nom du playbook à lancer
pour déployer votre travail.**

Assurez-vous d'avoir commité tout votre travail.  Ensuite, pour exporter votre dépôt git :

    git archive --prefix "tp-ansible/" -o /tmp/tp-ansible-NOM.tar.gz HEAD

Pour être sûr que l'archive fonctionne, vous pouvez tester de l'extraire dans un autre dossier et lancer Ansible avec :

    cd /tmp/
    tar xvf /tmp/tp-ansible-NOM.tar.gz
    cd tp-ansible
    # "vagrant up" (si besoin)
    ansible-playbook -i INVENTAIRE PLAYBOOK.yaml


## Quizz
| -                                 | =                             | +                          |
|-----------------------------------|-------------------------------|----------------------------|
| **Moins de la moitié** des points | **Moitié** des points ou plus | **3/4** des points ou plus |
