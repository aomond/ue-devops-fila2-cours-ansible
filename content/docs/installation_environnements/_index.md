---
title: "Installation des environnements"
description: ""
---

# Installation des environnements
## Installation de Virtualbox
- Identifier le package Virtualbox qui correspond à votre installation parmi cette liste: https://www.virtualbox.org/wiki/Download_Old_Builds_6_1
  - Pour ceux sur Windows, le package est: `VirtualBox-6.1.42-155177-Win.exe` (Windows hosts)
  - Pour ceux sur MacOS, le package est `VirtualBox-6.1.42-155177-OSX.dmg` (macOS / Intel hosts)
- Récupérer et installer / exécuter le package / l'installation.

## Installation de Vagrant
Suivre les instructions officielles: https://developer.hashicorp.com/vagrant/downloads

## Pour les machines Windows: installation de WSL
Le contrôleur Ansible ne fonctionne que sur Linux et macOS. Pour les machines Windows, nous ferons le TP sur WSL:

Cliquer sur **Windows** -> Rechercher **Activer ou désactiver des fonctionnalités Windows**
- Sélectionner **Sous-système Windows pour Linux**

Ouvrir **Powershell** en mode administrateur:
- Installer la distribution Ubuntu: ```wsl --install -d Ubuntu```
- Rentrer un username et un mot de passe

[//]: # (## Remote developpement)
[//]: # (### **VSCode**)
[//]: # (Tutoriel: https://code.visualstudio.com/docs/remote/wsl#_getting-started)
[//]: # ()
[//]: # (### **Pycharm**)
[//]: # (WSL suffit si vous développez sur VSCode. Pour Pycharm en revanche il faut passer sur WSL2:)
[//]: # (- Activer la virtualisation dans le BIOS)
[//]: # (- Cliquer sur **Windows** -> Rechercher **Activer ou désactiver des fonctionnalités Windows**)
[//]: # (  - Sélectionner **Hyper-V**)
[//]: # ()
[//]: # (- Ouvrir **Powershell** en mode administrateur:)
[//]: # (  - Mettre à jour WSL: `wsl --update`)
[//]: # (  - Mettre à jour la distribution Linux vers wsl2: `wsl --set-version Ubuntu 2`)
[//]: # (  - Mettre à jour la version par défaut de wsl: `wsl --set-default 2`)
[//]: # (Tutoriel: https://www.jetbrains.com/help/pycharm/remote-development-starting-page.html#run_in_wsl_ij)
[//]: # ()
[//]: # (La suite des installations se fait sur **WSL, Mac** ou **Linux**)

## Installation de pip
Vérifier si pip est installé: ```python3 -m pip -V```

Sinon, installer pip:
  ```
  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
  python3 get-pip.py
  ```

## Installation d'Ansible
Installer Ansible via pip: ```python3 -m pip install ansible```

Vérifier que Ansible est bien installé et accessible: ```ansible --version```

## Récupération du repo du TP
Forker le repository du TP: https://gitlab.imt-atlantique.fr/ue-devops-fila2/tp-ansible

