---
title: "Création du frontend"
description: ""
---

# Déploiement du frontend
## Télécharger le code source du service sur le noeud
**Modules à utiliser:** [git](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/git_module.html#ansible-collections-ansible-builtin-git-module)

1) Cloner le repository git contenant le code source du microservice ```frontend```: `https://gitlab+deploy-token-34:rcyUc-DqpMaoCmP62dfS@gitlab.imt-atlantique.fr/ue-devops-fila2/online-boutique-services/frontend`.
Le mettre dans le home du user root ```/root```\
   **Vérification**: ```ls /root/frontend```\
_Note_: ```gitlab+deploy-token-34``` et ```rcyUc-DqpMaoCmP62dfS``` sont respectivement le nom et la valeur d'un deploy token associé au repository.
Ce token permet de s'authentifier sur Gitlab sans avoir à passer par une authentification personnelle. Cela est courramment utilisé dans les scripts d'automatisation.

## Installation de Go
**Modules à utiliser:** [unarchive](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/unarchive_module.html#ansible-collections-ansible-builtin-unarchive-module),
[file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#ansible-collections-ansible-builtin-file-module),
[shell](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html#ansible-collections-ansible-builtin-shell-module)

2) Récupérer et dézipper l'achive contenant la version 1.19.3 de go: `https://go.dev/dl/go1.19.3.linux-amd64.tar.gz` dans le dossier de votre choix (exemple: `/usr/local`)\
   **Vérification**: ```ls /usr/local/go```
3) Rendre l'installation de Go accessible en ajoutant un lien symbolique situé dans `/usr/local/bin` pointant vers les exécutables de Go: `/usr/local/go/bin`\
   **Vérification**: ```go version```
4) Installer les dépendances go du service via la commande `go mod download`. Cette commande doit être lancée dans le dossier contenant le fichier `go.mod`.
   **Vérification**: ```ls ~/go/pkg/mod```

## Compilation du code source
**Modules à utiliser:** [apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#ansible-collections-ansible-builtin-apt-module),
[file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html#ansible-collections-ansible-builtin-file-module),
[shell](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/shell_module.html#ansible-collections-ansible-builtin-shell-module)

5) Installer les paquets pour compiler les fichiers `.proto`: `protobuf-compiler` et `golang-goprotobuf-dev`\
   **Vérification**: ```protoc --version```
6) Créer le dossier de destination des fichiers `.proto` compilés: `/root/frontend/genproto`\
   **Vérification**:  `ls /root/frontend/genproto`

7) Le frontend communique en gRPC avec les services recommendation, productcatalog et cart. Il faut donc compiler les trois fichiers .proto correspondant à ces trois services. 
Lancer la commande de compilation des prototypes gRPC en Go: `protoc --go_out=plugins=grpc:genproto -I /root/frontend /root/frontend/recommendationservice.proto /root/frontend/productcatalogservice.proto /root/frontend/cart.proto`. 
Cette commande est à lancer dans le dossier contenant les fichiers .proto: `/root/frontend`
   **Vérification**: `ls /root/frontend/recommendationservice.proto /root/frontend/productcatalogservice.proto /root/frontend/cart.proto` 

8) Lancer la commande `go build` pour compiler le projet. Cette commande doit prendre en argument la destination du projet compilé: `/var/frontend`. Cette commande est à lancer dans le dossier contenant les fichiers sources du services: `/root/frontend`
   **Vérification**: `ls /var/frontend` 

## Lancement du service
**Modules à utiliser:** [template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html#ansible-collections-ansible-builtin-template-module),
[systemd](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_module.html#ansible-collections-ansible-builtin-systemd-module),

Pour lancer le service, nous utiliserons `systemd`. Il s'agit d'un gestionnaire de services Linux permettant de démarrer, arrêter et monitorer des services utilisateur ou système. 
Cet outil se base sur un fichier de configuration décrivant le service à lancer, qui peut être démarré à la demande de l'utilisateur.
9) Créer un fichier de configuration que vous appelerez `online_boutique_service.service.j2` dont le contenu est le suivant:
```
[Unit]
Description=Lancement du microservice {{ service_name }}
After=network.target

[Service]
User=root
WorkingDirectory={{ microservice_source_dir }}
ExecStart=/var/{{ service_name }}
Environment=PRODUCT_CATALOG_SERVICE_ADDR=<ip_productcatalogservice>:3550 CART_SERVICE_ADDR=<ip_cartservice>:7070 RECOMMENDATION_SERVICE_ADDR=<ip_recommendationservice>:8080

[Install]
WantedBy=multi-user.target

```
Il s'agit d'un template d'un fichier de configuration `systemd`. Les variables `{{ service_name }}` et `{{ microservice_source_dir }}` seront **automatiquement** complétées lors de l'appel du module Ansible correspondant dans la tâche.
Ces variables devront donc être définies en amont avant l'exécution du playbook Ansible, tel que:
- `service_name` est le nom du service
- `microservice_source_dir` est le chemin vers le code source du service (code non compilé)
- `ip_<service>` est **à remplir vous-même** directement dans le fichier, il contient les IP de chacune des VMs hébergeant les services cart, productcatalog et recommendation

L'appel du module Ansible correspond créer un fichier contenant le template remplit avec les variables. Ce template remplit devra être créé dans le fichier suivant: `/etc/systemd/system/frontend.service`\
   **Vérification**: `cat /etc/systemd/system/frontend.service`


10) Lancer le service systemd en appelant le service `frontend`. Mettre l'option `daemon_reload: true` de façon à dire à `systemd` de recharger le nouveau fichier de config avant chaque lancement du service.\
    **Vérification**: `systemctl status frontend`

## Lancement de l'application
11) Vérifier si votre application s'est bien lancée, elle est hébergée sur le port `8080`. S'il n'y a pas d'erreurs, vous devez être capable de réaliser ces actions:
TODO: conditions de validation (actions à faire sur le site)
