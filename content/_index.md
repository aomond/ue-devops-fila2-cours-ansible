---
title: "UE DevOps FIL A2 2022-2023 - IMT Atlantique"
description: "Visualisation des séances"
bookToC: false
---

# UE DevOps FIL A2 2022-2023 - Cours/TP Ansible

# Préambule
- [Slides d'introduction](/slides/1-intro-ansible/1-intro-ansible.pdf)
- [Installation des environnements]({{< ref "docs/installation_environnements/_index.md" >}} "Installation des environnements" )
- [Plan des séances]({{< ref "docs/deroule_tp/_index.md" >}})
- [Slides Ansible réutilisable](/slides/2-reusable-ansible/2-reusable-ansible.pdf)

# TP: Déploiement du fil rouge
- [Evaluation]({{< ref "docs/presentation_fil_rouge/_index.md" >}} "Evaluation")
- [Déploiement de l'architecture]({{< ref "docs/deploiement_architecture/_index.md" >}} "Déploiement de l'architecture")
- [Rédaction des scripts Ansible]({{< ref "docs/redaction_ansible/_index.md" >}})
- [Troubleshooting]({{< ref "docs/infrastructure/_index.md" >}} "Troubleshooting")
